import { ReactWrapper } from 'enzyme';

// Tested with Antd Input, TextArea, DatePicker
function getFormFieldValue(form: ReactWrapper, field: string) {
  const wrapper = form.findWhere(el => {
    const fieldData = el.props()['data-__field'];

    return fieldData && fieldData.name === field;
  });

  if (wrapper.length > 0) {
    return wrapper.at(0).props().value;
  }
  // if (wrapper.length > 1) {
  //   throw new Error(`multiple fields with id ${field} found`);
  // }

  throw new Error(`field with id ${field} does not exist on Form component`);
}

export { getFormFieldValue };
