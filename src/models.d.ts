declare interface Charity {
  id?: string;
  name: string;
  description: string;
  logo: {
    id: string;
    fileName?: string;
    url?: string;
  };
}

interface Campaign {
  id?: string;
  charity: string | Charity;
  title: string;
  active: boolean;
  endDate: Date;
  details: CampaignDetails;
  updatedAt?: Date;
  createdAt?: Date;
  fundraising: FundRaising;
}

type Currency = 'USD';

interface FundRaising {
  goal: {
    currency: Currency;
    value: number;
  };
}

interface CampaignDetails {
  summary: string;
  description: string;
  areaAffected: string;
  peopleAffected: string;
  helpRequired: string;
  location: Location;
  images: { main: { id: string; fileName: string } };
}

declare interface Location {
  lat: number;
  lng: number;
  zoom: number;
  radius: number;
}

declare interface User {
  name: string;
  expiresAt?: number;
  accessToken: string;
  IDToken: string;
}

export { User, Campaign, Charity };
