import history from '../modules/history';
import log from '../modules/log';
import { addCampaign, uploadImage, updateCampaign, deleteCampaign as deleteCampaignAPI } from '../modules/api';
import C from '../constants';
import * as models from '../models';
import { Action } from 'redux';
import { default as store } from '../modules/store';
import { getUrl } from '../modules/utils';
import config from '../modules/config';

interface CreateCampaignAction {
  type: string;
  campaign?: models.Campaign;
  err?: any;
}

type CampaignAction = CreateCampaignAction;

// Create a models.Campaign

const actions = {
  addCampaignRequest() {
    return {
      type: C.ADD_CAMPAIGN_REQUEST
    };
  },

  addCampaignReceive(campaign: models.Campaign) {
    return {
      type: C.ADD_CAMPAIGN_RECEIVE,
      campaign
    };
  },

  addCampaignError(err: any) {
    return {
      type: C.ADD_CAMPAIGN_ERROR,
      err
    };
  },

  deleteCampaignRequest() {
    return {
      type: C.DELETE_CAMPAIGN_REQUEST,
    }
  },

  deleteCampaignReceive(id: string) {
    return {
      type: C.DELETE_CAMPAIGN_RECEIVE,
      id,
    }
  },

  deleteCampaignError(err: any) {
    return {
      type: C.DELETE_CAMPAIGN_ERROR,
      err,
    }
  }
};

// List Campaigns

function fetchCampaignsRequest(): Action {
  return {
    type: C.FETCH_CAMPAIGNS_REQUEST
  };
}

function fetchCampaignsReceive(campaigns: models.Campaign[]) {
  return {
    campaigns,
    type: C.FETCH_CAMPAIGNS_RECEIVE
  };
}

function fetchCampaignsError(err: any) {
  return {
    err,
    type: C.FETCH_CAMPAIGNS_ERROR
  };
}
interface FetchCampaignsCriteria {
  searchName: string;
}

// Update Campaign
function updateCampaignRequest(): Action {
  return {
    type: C.UPDATE_CAMPAIGN_REQUEST
  };
}

function updateCampaignReceive(campaign: models.Campaign) {
  return {
    type: C.UPDATE_CAMPAIGN_RECEIVE,
    campaign
  };
}

function updateCampaignError(err: any) {
  return {
    type: C.UPDATE_CAMPAIGN_ERROR,
    err
  };
}

const campaignAdd = (campaign: models.Campaign, image: File) => {
  return (dispatch: any) => {
    dispatch(actions.addCampaignRequest());

    uploadImage(image)
      .then(resp => {
        if (resp.ok) {
          return resp.json();
        }
        const mess = 'unexpected response posting photo';
        throw new Error(mess);
      })
      .then(json => {
        campaign.details.images.main.id = json.id;
        campaign.details.images.main.fileName = json.fileName;
        return addCampaign(campaign);
      })
      .then(resp => {
        if (!resp.ok) {
          throw Error('error posting campaign');
        }
        return resp.json();
      })
      .then(json => {
        dispatch(actions.addCampaignReceive(json));
        history.push('/charities');
      })
      .catch((err: any) => {
        log.error(err);
        dispatch(actions.addCampaignError(err));
      });
  };
};

const deleteCampaign = (id: string) => {
  return (dispatch: any) => {
    dispatch(actions.deleteCampaignRequest());
    deleteCampaignAPI(id)
    .then((res: Response) => {
      if (res.status >= 400) {
        dispatch(actions.deleteCampaignError('Campaign wasn\'t deleted'));
        return;
      }
      dispatch(actions.deleteCampaignReceive(id));
    })
    .catch((err) => {
      log.error(err);
      dispatch(actions.deleteCampaignError(err));
    });
  }
};

function fetchCampaigns(criteria?: FetchCampaignsCriteria, inactive?: boolean) {
  const params: any = {
    criteria: criteria ? criteria : undefined,
    inactive: inactive ? inactive : undefined
  };
  return (dispatch: any) => {
    dispatch(fetchCampaignsRequest());
    fetch(getUrl(`${config.API_BASE}campaigns`, params))
      .then(resp => {
        if (!resp.ok) {
          store.dispatch(fetchCampaignsError('Server error'));
          return;
        }
        return resp.json();
      })
      .then(json => {
        store.dispatch(
          fetchCampaignsReceive(json.items.map((campaign: any) => campaign))
        );
      })
      .catch(err => {
        store.dispatch(fetchCampaignsError(err));
      });
  };
}

function campaignUpdate(campaign: models.Campaign, image: File) {
  return (dispatch: any) => {
    dispatch(updateCampaignRequest());
    if (image) {
      uploadImage(image)
        .then(resp => {
          if (resp.ok) {
            return resp.json();
          }
          const mess = 'unexpected response posting photo';
          throw new Error(mess);
        })
        .then(json => {
          campaign.details.images.main.id = json.id;
          campaign.details.images.main.fileName = json.fileName;
          return updateCampaign(campaign);
        })
        .then(resp => {
          if (!resp.ok) {
            throw Error('Error posting campaign');
          }
          return resp.json();
        })
        .then(json => {
          dispatch(updateCampaignReceive(json));
          history.push('/campaigns');
        })
        .catch((err: any) => {
          log.error(err);
          dispatch(updateCampaignError(err));
        });
    } else {
      updateCampaign(campaign)
        .then(resp => {
          if (!resp.ok) {
            throw Error('Error posting campaign');
          }
          return resp.json();
        })
        .then(json => {
          dispatch(updateCampaignReceive(json));
          history.push('/campaigns');
        })
        .catch((err: any) => {
          log.error(err);
          dispatch(updateCampaignError(err));
        });
    }
  };
}

export { campaignAdd, CampaignAction, fetchCampaigns, campaignUpdate, deleteCampaign };
