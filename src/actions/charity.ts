import * as url from 'url';
import { Action } from 'redux';

import { Charity } from '../models';
import history from '../modules/history';
import { getAuthHeader } from '../modules/auth';
import log from '../modules/log';
import config from '../modules/config';
import C from '../constants';
import { default as store, CharityStore } from '../modules/store';

interface CharityAction {
  type: string;
  charities: CharityStore;
  err?: any;
}
// List Charities

function fetchCharitiesRequest(): Action {
  return {
    type: C.FETCH_CHARITIES_REQUEST
  };
}

function fetchCharitiesReceive(charities: Charity[]) {
  return {
    charities,
    type: C.FETCH_CHARITIES_RECEIVE
  };
}

function fetchCharitiesError(err: any) {
  return {
    err,
    type: C.FETCH_CHARITIES_ERROR
  };
}
interface FetchCharitiesCriteria {
  searchName: string;
}

// Create a Charity

function addCharityRequest() {
  return {
    type: C.ADD_CHARITY_REQUEST
  };
}

function addCharityReceive(charity: Charity) {
  return {
    type: C.ADD_CHARITY_RECEIVE,
    charity
  };
}

function addCharityError(err: any) {
  return {
    type: C.ADD_CHARITY_ERROR,
    err
  };
}

// Update Charity

function updateCharityRequest() {
  return {
    type: C.UPDATE_CHARITY_REQUEST
  };
}

function updateCharityReceive(charity: Charity) {
  return {
    type: C.UPDATE_CHARITY_RECEIVE,
    charity
  };
}

function updateCharityError(err: any) {
  return {
    type: C.UPDATE_CHARITY_ERROR,
    err
  };
}

// Action functions

function fetchCharities(criteria?: FetchCharitiesCriteria) {
  return (dispatch: any) => {
    dispatch(fetchCharitiesRequest());

    fetch(url.resolve(config.API_BASE, 'charities'))
      .then(resp => {
        if (!resp.ok) {
          store.dispatch(fetchCharitiesError);
          return;
        }
        return resp.json();
      })
      .then(json => {
        store.dispatch(fetchCharitiesReceive(json));
      })
      .catch(err => {
        store.dispatch(fetchCharitiesError(err));
      });
  };
}

function updateCharity(charity: Charity, logo: any) {
  return (dispatch: any) => {
    dispatch(updateCharityRequest());

    const postUpdate = (update: Charity) => {
      return fetch(url.resolve(config.API_BASE, `charities/${charity.id}`), {
        method: 'PATCH',
        headers: {
          accept: 'application/json',
          'content-type': 'application/json',
          Authorization: getAuthHeader()
        },
        body: JSON.stringify(update)
      })
        .then(resp => {
          if (!resp.ok) {
            throw Error('error posting charity');
          }
          return resp.json();
        })
        .then(json => {
          dispatch(updateCharityReceive(json));
          history.push('/charities');
        });
    };

    if (logo) {
      fetch(url.resolve(config.API_BASE, 'photos'), {
        method: 'POST',
        headers: {
          'content-type': 'application/octet'
        },
        body: logo
      })
        .then(resp => {
          if (resp.ok) {
            return resp.json();
          }
          const mess = 'unexpected response posting photo';
          throw new Error(mess);
        })
        .then(json => {
          charity.logo = { id: json.id, fileName: json.fileName };
          return postUpdate(charity);
        })
        .catch(err => {
          log.error(err);
          dispatch(updateCharityError(err));
        });
    } else {
      postUpdate(charity).catch(err => {
        dispatch(updateCharityError(err));
      });
    }
  };
}

// addCharity first sends a request to upload the image data.
// The returned image id is used in the request to create the Charity.
function addCharity(charity: Charity, logo: File) {
  return (dispatch: any) => {
    dispatch(addCharityRequest());
    fetch(url.resolve(config.API_BASE, 'photos'), {
      method: 'POST',
      headers: {
        Authorization: getAuthHeader(),
        'content-type': 'application/octet-stream'
      },
      body: logo
    })
      .then(resp => {
        if (resp.ok) {
          return resp.json();
        }
        log.error(resp);
        const mess = 'failed to upload photo';
        throw new Error(mess);
      })
      .then(json => {
        charity.logo = { id: json.id, fileName: json.fileName };
        return fetch(url.resolve(config.API_BASE, 'charities'), {
          method: 'POST',
          headers: {
            accept: 'application/json',
            'content-type': 'application/json',
            Authorization: getAuthHeader()
          },
          body: JSON.stringify(charity)
        });
      })
      .then(resp => {
        if (!resp.ok) {
          throw Error('unexpected response posting charity');
        }
        return resp.json();
      })
      .then(json => {
        dispatch(addCharityReceive(json));
        history.push('/charities');
      })
      .catch(err => {
        log.error(err);
        dispatch(addCharityError(err));
      });
  };
}

export { fetchCharities, addCharity, updateCharity, CharityAction };
