import Constants from '../constants';
import config from '../modules/config';

import log from '../modules/log';

function getDonationsInProgress(startId?: number) {
  return {
    type: Constants.FETCH_DONATIONS_REQUEST,
    startId
  };
}

function getDonationsResolved(donations: any, nextId: any) {
  return {
    type: Constants.FETCH_DONATIONS_RECEIVE,
    donations,
    nextId
  };
}

function getDonationsRejected(err?: any) {
  return {
    type: Constants.FETCH_DONATIONS_ERROR
  };
}

function getDonationsURL(urlParams: any) {
  const donationsUrl = new URL(`${config.API_BASE}donations`);
  const query = donationsUrl.searchParams;

  Object.keys(urlParams).forEach(param => {
    const val = urlParams[param];
    if (val) {
      query.append(param, val);
    }
  });

  log.debug('donations url: ', donationsUrl.toString());
  return donationsUrl.toString();
}

function getDonations(startId?: number, count: number = 100) {
  return (dispatch: any, getState: any) => {
    const donationStore = getState().donations;

    const urlParams: any = {
      startId: startId ? startId : undefined,
      count,
      name: donationStore.searchText,
      amountMin: donationStore.isAmountFilterEnabled
        ? donationStore.amountFilter[0]
        : undefined,
      amountMax: donationStore.isAmountFilterEnabled
        ? donationStore.amountFilter[1]
        : undefined,
      dateStart: donationStore.dateFilter
        ? donationStore.dateFilter[0]
        : undefined,
      dateEnd: donationStore.dateFilter
        ? donationStore.dateFilter[1]
        : undefined
    };

    dispatch(getDonationsInProgress(startId));

    return fetch(getDonationsURL(urlParams))
      .then(resp => {
        if (resp.ok) {
          return resp.json();
        }
        throw Error('unexpected response fetching donations');
      })
      .then(resp => {
        dispatch(getDonationsResolved(resp.value, resp.nextId));
      })
      .catch(err => {
        dispatch(getDonationsRejected());
      });
  };
}

function setSearchText(searchText: string) {
  return {
    type: Constants.SET_SEARCH_TEXT,
    searchText
  };
}

function setAmountFilter(amountFilter: number[]) {
  return {
    type: Constants.SET_AMOUNT_FILTER,
    amountFilter
  };
}

function setDateFilter(dateFilter: number[]) {
  return {
    type: Constants.SET_DATE_FILTER,
    dateFilter
  };
}

function toggleAmountFilter(enableFilter: boolean) {
  return {
    type: Constants.TOGGLE_AMOUNT_FILTER,
    enableFilter
  };
}

export default {
  getDonations,
  setSearchText,
  setAmountFilter,
  setDateFilter,
  toggleAmountFilter
};
