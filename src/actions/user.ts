import { extend } from '../modules/types';
import C from '../constants';
import { User } from '../models';
import store from '../modules/store';

interface UserAction {
  type: string;
  user: User | null;
}

const logout = () => {
  const action: UserAction = extend({ type: C.REMOVE_LOGIN }, { user: null });
  store.dispatch(action);
};

const login = (user: User) => {
  const action: UserAction = extend({ type: C.ADD_LOGIN }, { user });
  store.dispatch(action);
};

export { logout, login, UserAction };
