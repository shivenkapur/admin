import log from './log';
import * as process from 'process';

// parceljs build handles replacement of process.env.* strings with shell values

const config = {
  API_BASE: process.env.REACT_APP_API_URL || 'http://localhost:3000/api/v1/',
  SECURED: process.env.REACT_APP_SECURED
    ? process.env.REACT_APP_SECURED === 'true'
    : true,
  AUTH0_CLIENT_ID: process.env.REACT_APP_AUTH0_CLIENT_ID || '',
  AUTH0_DOMAIN: process.env.REACT_APP_AUTH0_DOMAIN || '',
  AUTH0_AUDIENCE_URL: process.env.REACT_APP_AUTH0_AUDIENCE_URL || ''
};

log.debug(config);

export default config;
