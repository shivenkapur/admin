import { History } from 'history';
import { Store } from 'redux';
import { WebAuth } from 'auth0-js';

import config from './config';
import log from './log';
import { logout, login } from '../actions/user';
import { UserStore } from './store';
import { User } from '../models';

function newWebAuth() {
  return new WebAuth({
    clientID: config.AUTH0_CLIENT_ID,
    domain: config.AUTH0_DOMAIN,
    redirectUri: `${window.location.protocol}//${window.location.host}/auth0`,
    audience: config.AUTH0_AUDIENCE_URL,
    responseType: 'token id_token',
    scope: 'openid'
  });
}

function expiresAt(expiresIn: number | undefined): number | undefined {
  if (expiresIn) {
    return expiresIn * 1000 + new Date().getTime();
  }
}

function isAuthenticated(user: UserStore) {
  log.debug(`Admin app secured: ${config.SECURED}`);
  const loggedIn = user.loggedIn;

  if (!loggedIn) {
    return false;
  }

  // check for authentication expiry
  if (loggedIn.expiresAt) {
    const expired = new Date().getTime() > loggedIn.expiresAt;
    log.debug(`${loggedIn.name} authentication expired: ${expired}`);
    return !expired;
  }
  log.debug(`${loggedIn.name} authentication has no expiry`);
  return true;
}

function handleAuthentication(
  webAuth: WebAuth,
  store: Store<any>,
  history: History
) {
  webAuth.parseHash((err, authResult) => {
    if (authResult && authResult.accessToken && authResult.idToken) {
      log.debug('successful login from auth0');
      const user: User = {
        name: 'Admin',
        IDToken: authResult.accessToken,
        accessToken: authResult.accessToken,
        expiresAt: expiresAt(authResult.expiresIn)
      };
      localStorage.setItem('user', JSON.stringify(user));
      login(user);
      history.push('/');
    } else if (err) {
      log.debug('unsuccessful login from auth0');
      log.debug(authResult);
      log.error(err);
      history.push('/loginError');
    }
  });
}

function handleLogout(store: Store<any>, history: History) {
  localStorage.removeItem('user');
  logout();
  history.push('/loggedOut');
}

function getAuthHeader() {
  const user = localStorage.getItem('user');
  return user ? `Bearer ${JSON.parse(user).accessToken}` : '';
}

export {
  isAuthenticated,
  handleAuthentication,
  newWebAuth,
  handleLogout,
  getAuthHeader
};
