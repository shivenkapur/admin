function extend<T, U>(first: T, second: U): T & U {
  const result : any = {};
  for (const id in first) {
    if (first.hasOwnProperty(id)) {
      (result as any)[id] = (first as any)[id];
    }
  }
  for (const id in second) {
    if (!result.hasOwnProperty(id)) {
      (result as any)[id] = (second as any)[id];
    }
  }
  return result as T & U;
}

export { extend };
