import * as url from 'url';

import config from '../modules/config';
import { getAuthHeader } from '../modules/auth';
import { Campaign } from '../models';

const addCampaign = (campaign: Campaign) => {
  return fetch(url.resolve(config.API_BASE, 'campaigns'), {
    method: 'POST',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      Authorization: getAuthHeader()
    },
    body: JSON.stringify(campaign)
  });
};

const deleteCampaign = (id: string) => {
  return fetch(url.resolve(config.API_BASE, `campaigns/${id}`), {
    method: 'DELETE',
    headers: {
      accept: 'application/json',
      Authorization: getAuthHeader(),
    },
  });
};

const updateCampaign = (campaign: Campaign) => {
  return fetch(url.resolve(config.API_BASE, `campaigns/${campaign.id}`), {
    method: 'PUT',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      Authorization: getAuthHeader()
    },
    body: JSON.stringify(campaign)
  });
};

const uploadImage = (image: File) => {
  return fetch(url.resolve(config.API_BASE, 'photos'), {
    method: 'POST',
    headers: {
      'content-type': 'application/octet-stream',
      Authorization: getAuthHeader()
    },
    body: image
  });
};

export { addCampaign, uploadImage, updateCampaign, deleteCampaign };
