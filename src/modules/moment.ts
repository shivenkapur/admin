// tslint:disable-next-line
import * as moment from 'moment';

const momentFunction =
 {}.toString.call(moment) === '[object Function]'
   ? moment
   : moment.default;

export default momentFunction;
