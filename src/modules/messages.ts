const messages = {
  SERVER_ERROR: 'An error occurred communicating with the server'
};

export default messages;
