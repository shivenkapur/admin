// const XmlHttpPostBinary = (postUrl: any, blob: any) => {
//   return new Promise((resolve, reject) => {
//     const req = new XMLHttpRequest();
//     req.overrideMimeType('application/octet-stream');
//     req.open('POST', postUrl, true);
//     req.onload = e => {
//       resolve();
//     };
//     req.onerror = e => {
//       reject(e);
//     };
//     req.send(blob);
//   });
// };
// XmlHttpPostBinary(url.resolve(config.API_BASE, 'photos'), logo)

const getUrl = (url: string, params: any) => {
  const queryUrl = new URL(url);
  const query = queryUrl.searchParams;

  Object.keys(params).forEach(param => {
    const val = params[param];
    if (val) {
      query.append(param, val);
    }
  });
  return queryUrl.toString();
};

export { getUrl };
