const log = {
  debug(...obj: any[]) {
    if (!(global as any).logDisabled) {
      // tslint:disable-next-line
      console.log(...obj);
    }
  },
  error(...obj: any[]) {
    if (!(global as any).logDisabled) {
      // tslint:disable-next-line
      console.error(obj);
    }
  }
};

export default log;
