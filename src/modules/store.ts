import {
  Store,
  applyMiddleware,
  createStore,
  Reducer,
  combineReducers
} from 'redux';
import thunk from 'redux-thunk';

import log from './log';
import userReducer from '../reducers/user';
import charitiesReducer from '../reducers/charity';
import campaignsReducer from '../reducers/campaign';
import donationReducers from '../reducers/donation';
import { User, Campaign } from '../models';

interface CharityStore {
  items: any[];
  isFetching: boolean;
  isCreating: boolean;
  isUpdating: boolean;
}

interface CampaignStore {
  items: Campaign[];
  isCreating: boolean;
  isUpdating: boolean;
}

interface UserStore {
  loggedIn: User | null;
}

interface AdminStore {
  charities: CharityStore;
  campaigns: CampaignStore;
  user: UserStore;
  donations: DonationStore;
}

interface DonationStore {
  isLoading: boolean;
  donations: any[];
  searchText: string;
  amountFilter: number[];
  isAmountFilterEnabled: boolean;
  dateFilter: any;
  nextIdToFetch: any;
}

const reducers = {
  user: userReducer as Reducer<UserStore>,
  charities: charitiesReducer,
  campaigns: campaignsReducer,
  donations: donationReducers
};

const store = createStore<AdminStore>(
  combineReducers<AdminStore>(reducers),
  // The thunk middleware allows Actions to be async (eg Ajax requests)
  applyMiddleware(thunk)
);

const addLoggingToDispatch = (dispatchStore: Store<any>) => {
  const rawDispatch = dispatchStore.dispatch;
  const newDispatch = (action: any) => {
    if (typeof action === 'function') {
      action(newDispatch, store.getState);
      return;
    }
    log.debug('state', store.getState());
    log.debug('action', action);
    rawDispatch(action);
    log.debug('next state', store.getState());
  };
  dispatchStore.dispatch = newDispatch;
  return newDispatch;
};

const dispatch = addLoggingToDispatch(store);

export default store;

export {
  dispatch,
  AdminStore,
  UserStore,
  CharityStore,
  CampaignStore,
  DonationStore
};
