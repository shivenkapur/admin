import C from '../constants';
import { UserStore } from '../modules/store';
import { UserAction } from '../actions/user';

const reducer = (
  state: UserStore = { loggedIn: null },
  action: UserAction
): UserStore => {
  switch (action.type) {
    case C.ADD_LOGIN: {
      return { ...state, loggedIn: action.user };
    }
    case C.REMOVE_LOGIN:
      return { ...state, loggedIn: null };
    default:
      return { ...state };
  }
};

export default reducer;
