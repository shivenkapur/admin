import { notification } from 'antd';
import C from '../constants';
import messages from '../modules/messages';
import { CampaignStore } from '../modules/store';

const defaultState: CampaignStore = {
  items: [],
  isCreating: false,
  isUpdating: false
};

const reducer = (
  state: CampaignStore = defaultState,
  action: any
): CampaignStore => {
  switch (action.type) {
    case C.UPDATE_CAMPAIGN_REQUEST:
      return { ...state, isUpdating: true };
    case C.UPDATE_CAMPAIGN_RECEIVE:
      notification.info({
        message: 'Campaign updated',
        description: `${action.campaign.title} was updated`
      });
      const updatedRemoved = state.items.filter(item => {
        return item.id !== action.campaign.id;
      });

      return {
        ...state,
        isUpdating: false,
        items: [...updatedRemoved, action.campaign]
      };
    case C.UPDATE_CAMPAIGN_ERROR:
      notification.error({
        message: 'Failed to update campaign',
        description: messages.SERVER_ERROR
      });
      return { ...state, isUpdating: false };
    case C.FETCH_CAMPAIGNS_REQUEST:
      return { ...state, isCreating: true };
    case C.FETCH_CAMPAIGNS_RECEIVE:
      return { ...state, items: action.campaigns };
    case C.FETCH_CAMPAIGNS_ERROR:
      notification.error({
        message: 'Failed to fetch campaigns',
        description: messages.SERVER_ERROR
      });
      return { ...state, isCreating: false };
    case C.ADD_CAMPAIGN_REQUEST:
      return { ...state, isCreating: true };
    case C.ADD_CAMPAIGN_RECEIVE:
      notification.info({
        message: 'Campaign created',
        description: `${action.campaign.title} was created`
      });
      return {
        ...state,
        isCreating: false,
        items: [...state.items, action.campaign]
      };
    case C.ADD_CHARITY_ERROR:
      notification.error({
        message: 'Failed to create campaign',
        description: messages.SERVER_ERROR
      });
      return { ...state, isCreating: false };
    
    case C.DELETE_CAMPAIGN_REQUEST:
      return {...state };

    case C.DELETE_CAMPAIGN_RECEIVE:
      const campaign = state.items.find(item => item.id === action.id);
      const updatedItems = state.items.filter(item => item.id !== action.id);
      notification.info({
        message: 'Campaign deleted',
        description: `${campaign ? campaign.title : 'Campaign'} was deleted`,
      });
      return { ...state, items: [...updatedItems] };

    case C.DELETE_CAMPAIGN_ERROR:
      notification.error({
        message: 'Failed to delete campaign',
        description: action.err,
      });
      return {...state};
  }
  return state;
};

export default reducer;
