import { notification } from 'antd';

import messages from '../modules/messages';
import { DonationStore } from '../modules/store';
import Constants from '../constants';

const defaultState = {
  isLoading: false,
  donations: [],
  searchText: '',
  isAmountFilterEnabled: false,
  amountFilter: [1, 100],
  dateFilter: [],
  nextIdToFetch: null
};

const reducer = (state: DonationStore = defaultState, action: any): any => {
  switch (action.type) {
    case Constants.FETCH_DONATIONS_REQUEST:
      return {
        ...state,
        isLoading: true,
        donations: action.startId ? state.donations : []
      };

    case Constants.FETCH_DONATIONS_RECEIVE:
      return {
        ...state,
        isLoading: false,
        donations: state.donations.concat(action.donations),
        nextIdToFetch: action.nextId || null
      };

    case Constants.FETCH_DONATIONS_ERROR:
      notification.error({
        message: 'Failed to get donations',
        description: messages.SERVER_ERROR
      });

      return { ...state, isLoading: false };

    case Constants.SET_SEARCH_TEXT:
      return { ...state, searchText: action.searchText };

    case Constants.SET_AMOUNT_FILTER:
      return { ...state, amountFilter: action.amountFilter };

    case Constants.SET_DATE_FILTER:
      return { ...state, dateFilter: action.dateFilter };

    case Constants.TOGGLE_AMOUNT_FILTER:
      return { ...state, isAmountFilterEnabled: action.enableFilter };
  }

  return state;
};

export default reducer;
