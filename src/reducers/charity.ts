import { notification } from 'antd';
import C from '../constants';
import messages from '../modules/messages';
import { CharityStore } from '../modules/store';

const defaultState: CharityStore = {
  items: [],
  isFetching: false,
  isCreating: false,
  isUpdating: false
};

const reducer = (
  state: CharityStore = defaultState,
  action: any
): CharityStore => {
  switch (action.type) {
    case C.FETCH_CHARITIES_REQUEST: {
      return { ...state, isFetching: true };
    }
    case C.FETCH_CHARITIES_RECEIVE:
      return { ...state, items: action.charities, isFetching: false };

    case C.FETCH_CHARITIES_ERROR:
      notification.error({
        message: 'Failed to fetch charities',
        description: messages.SERVER_ERROR
      });
      return { ...state, isFetching: false };

    case C.ADD_CHARITY_REQUEST:
      return { ...state, isCreating: true };

    case C.ADD_CHARITY_RECEIVE:
      notification.info({
        message: 'Charity created',
        description: `${action.charity.name} was created`
      });
      return {
        ...state,
        isCreating: false,
        items: [...state.items, action.charity]
      };

    case C.ADD_CHARITY_ERROR:
      notification.error({
        message: 'Failed to create charity',
        description: messages.SERVER_ERROR
      });
      return { ...state, isCreating: false };

    case C.UPDATE_CHARITY_REQUEST:
      return { ...state, isUpdating: true };

    case C.UPDATE_CHARITY_RECEIVE:
      notification.info({
        message: 'Charity updated',
        description: `${action.charity.name} was updated`
      });
      const updatedRemoved = state.items.filter(item => {
        return item.id !== action.charity.id;
      });
      return {
        ...state,
        isUpdating: false,
        items: [...updatedRemoved, action.charity]
      };

    case C.UPDATE_CHARITY_ERROR:
      notification.error({
        message: 'Failed to create charity',
        description: messages.SERVER_ERROR
      });
      return { ...state, isUpdating: false };
  }
  return state;
};

export default reducer;
