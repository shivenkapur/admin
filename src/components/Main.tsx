import * as React from 'react';
import { Route, RouteComponentProps } from 'react-router';
import { mapProps } from 'recompose';
import { Store } from 'redux';

import { Layout, Menu } from 'antd';

import { dispatch } from '../modules/store';
import { fetchCharities } from '../actions/charity';
import { fetchCampaigns } from '../actions/campaigns';
import CharityOperations from './charity/CharityOperations';
import CampaignOperations from './campaign/CampaignOperations';
import CampaignSubMenu from '../components/campaign/CampaignSubMenu';
import DonationsList from '../components/DonationsList';
import CharitySubMenu from './charity/CharitySubMenu';

import { handleLogout } from '../modules/auth';

const { Header, Sider } = Layout;

interface MainMenuProps extends RouteComponentProps<any> {
  store: Store<any>;
  handleClick: (e: any) => void;
}

const MainMenu = (props: MainMenuProps) => {
  return (
    <Menu
      theme="dark"
      mode="horizontal"
      style={{ lineHeight: '64px' }}
      onClick={props.handleClick}
    >
      <Menu.Item key="charities">Charities</Menu.Item>
      <Menu.Item key="campaigns">Campaigns</Menu.Item>
      <Menu.Item key="donations">Donations</Menu.Item>
      <Menu.Item key="logout">Logout</Menu.Item>
    </Menu>
  );
};

const addProps = mapProps((props: MainMenuProps) => {
  return {
    ...props,
    handleClick: (e: any) => {
      switch (e.key) {
        case 'charities':
          dispatch(fetchCharities());
          props.history.push('/charities');
          break;
        case 'campaigns':
          dispatch(fetchCampaigns());
          props.history.push('/campaigns');
          break;
        case 'donations':
          props.history.push('/donations');
          break;
        case 'logout':
          handleLogout(props.store, props.history);
      }
    }
  };
});

const ReMainMenu = addProps(MainMenu);

const Main = (props: any) => {
  const renderSubMenu = (propsIn: any) => <CharitySubMenu {...propsIn} />;
  const renderCampaignSub = (propsIn: any) => <CampaignSubMenu {...propsIn} />;
  return (
    <div>
      <Layout>
        <Header className="headerClass">
          <div className="logo" />
          <ReMainMenu {...props} />
        </Header>
        <Layout>
          <Sider width={200} style={{ background: '#fff' }}>
            <Route path="/charities" render={renderSubMenu} />
            <Route path="/campaigns" render={renderCampaignSub} />
          </Sider>
          <Layout style={{ padding: '0 24px 24px' }}>
            <CharityOperations {...props} />
            <CampaignOperations {...props} />
            <Route path="/donations" component={DonationsList} />
          </Layout>
        </Layout>
      </Layout>
    </div>
  );
};

export default addProps(Main);
