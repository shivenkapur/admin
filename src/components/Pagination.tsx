import * as React from 'react';

interface PaginationProps {
  isVisible: boolean;
  startPage: number;
  endPage: number;
  currentPage: number;
  goToPage: any;
  hasReachedEnd: boolean;
}

const render = (props: PaginationProps) => {
  const controlClickHandler = (controlType: string) => {
    let nextPage = props.currentPage || 1;
    if (controlType === 'prev') {
      nextPage--;
    } else if (controlType === 'next') {
      nextPage++;
    }

    props.goToPage(nextPage);
  };

  const getPages = () => {
    const pages = [];
    for (let page = props.startPage; page <= props.endPage; page++) {
      pages.push(
        <div
          key={page}
          className={`page-item ${props.currentPage === page ? 'current' : ''}`}
          onClick={props.goToPage.bind(undefined, page)}
        >
          {page}
        </div>
      );
    }
    return pages;
  };

  return (
    <div className={`pagination-container ${props.isVisible ? 'visible' : ''}`}>
      {props.currentPage > 1 && (
        <div
          className="control control-prev"
          onClick={controlClickHandler.bind(undefined, 'prev')}
        >
          Previous
        </div>
      )}

      <div className="pages-wrapper">{getPages()}</div>

      {!props.hasReachedEnd && (
        <div
          className="control control-next"
          onClick={controlClickHandler.bind(undefined, 'next')}
        >
          Next
        </div>
      )}
    </div>
  );
};

export default render;
