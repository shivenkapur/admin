import * as React from 'react';
import { connect } from 'react-redux';
import { compose, lifecycle, withHandlers, withState } from 'recompose';
import { bindActionCreators } from 'redux';

import { Input, DatePicker, Button, Slider, Collapse, Checkbox } from 'antd';
const Search = Input.Search;
const { RangePicker } = DatePicker;
const Panel = Collapse.Panel;

import { dispatch } from '../modules/store';
import DonationActions from '../actions/donation';

import PaginatedList from './PaginatedList';

const MAX_PAGES_TO_SHOW = 10;
const PAGE_SIZE = 10;

const columnsToShow = [
  {
    title: 'User Name',
    dataIndex: 'donorDetails.name',
    key: 'name'
  },
  {
    title: 'Amount',
    dataIndex: 'amount',
    key: 'amount'
  },
  {
    title: 'Date',
    dataIndex: 'madeAt',
    key: 'madeAt',
    render: (text: any) => <span>{new Date(text).toLocaleString()}</span>
  }
];

const marksForSlider = {
  0: '1$',
  100: '100$'
};

const handlers = withHandlers({
  onSearchTextChange: (props: any) => (event: any) => {
    props.setSearchText(event.target.value);
  },

  onDateRangeFilterChange: (props: any) => (
    momentDates: any[],
    dateStrings: string[]
  ) => {
    if (dateStrings[0]) {
      props.setDateFilter([
        new Date(dateStrings[0]).getTime(),
        new Date(dateStrings[1]).getTime()
      ]);
    } else {
      props.setDateFilter([]);
    }
  },

  onAmountFilterValueChange: (props: any) => (value: any) => {
    props.setAmountFilter(value);
  },

  toggleAmountFilter: (props: any) => (event: any) => {
    props.toggleAmountFilter(event.target.checked);
  },

  onUpdateResults: (props: any) => (event: any) => {
    props.setPagination({
      start: 1,
      end: 10,
      current: 1
    });

    props.getDonations();
  },

  goToPage: (props: any) => (pageNumberRequested: number) => {
    const { start, end } = props.pagination;
    const midPage = start + Math.floor((end - start) / 2);
    const shiftPagesBy = pageNumberRequested - midPage - 1;

    const nextPaginationCouldBe = {
      start,
      end,
      current: pageNumberRequested
    };

    if (
      props.nextIdToFetch &&
      !props.donations[end + shiftPagesBy] &&
      pageNumberRequested > midPage
    ) {
      props.getDonations(props.nextIdToFetch, PAGE_SIZE * shiftPagesBy);
    }

    if (
      (pageNumberRequested > props.pagination.current &&
        pageNumberRequested > midPage) ||
      (pageNumberRequested < props.pagination.current &&
        pageNumberRequested < midPage)
    ) {
      const nextStart = start + shiftPagesBy > 1 ? start + shiftPagesBy : 1;
      let nextEnd = end + shiftPagesBy;

      if (nextStart === 1) {
        nextEnd = MAX_PAGES_TO_SHOW;
      }

      nextPaginationCouldBe.start = nextStart;
      nextPaginationCouldBe.end = nextEnd;
    }

    props.setPagination(nextPaginationCouldBe);
  }
});

const sliderValueFormatter = (value: any) => {
  return `${value}$`;
};

const getDonationsOnMount = lifecycle({
  componentDidMount() {
    dispatch(DonationActions.getDonations());
  }
});

const render = (props: any) => {
  const pagination = { ...props.pagination };
  let isLastPageVisible = false;
  if (!props.nextIdToFetch) {
    const lastPageShouldBe = Object.keys(props.donations).length;
    if (lastPageShouldBe > MAX_PAGES_TO_SHOW) {
      if (lastPageShouldBe - pagination.current < MAX_PAGES_TO_SHOW / 2 - 1) {
        pagination.start = lastPageShouldBe - MAX_PAGES_TO_SHOW + 1;
        pagination.end = lastPageShouldBe;
      }
    } else {
      pagination.start = 1;
      pagination.end = lastPageShouldBe;
    }

    if (pagination.current === lastPageShouldBe) {
      isLastPageVisible = true;
    }
  }

  return (
    <div className="donations-list">
      <div className="section">
        <Collapse>
          <Panel header="Search and Filter" key="1">
            <div className="">
              <div className="search-section" style={{ marginBottom: 16 }}>
                <Search
                  value={props.searchText}
                  placeholder="Search by name"
                  onChange={props.onSearchTextChange}
                  style={{ width: 280 }}
                />
              </div>

              <div
                className="filter-section"
                style={{ width: '100%', marginBottom: 16 }}
              >
                <span
                  style={{
                    display: 'inline-block',
                    verticalAlign: 'middle',
                    marginRight: 40
                  }}
                >
                  <span
                    style={{
                      fontWeight: 'bold',
                      display: 'inline-block',
                      verticalAlign: 'middle',
                      marginRight: 12
                    }}
                  >
                    Date
                  </span>
                  <span
                    style={{ display: 'inline-block', verticalAlign: 'middle' }}
                  >
                    <RangePicker
                      format={'YYYY/MM/DD'}
                      style={{ width: 280 }}
                      onChange={props.onDateRangeFilterChange}
                    />
                  </span>
                </span>
                <span
                  style={{ display: 'inline-block', verticalAlign: 'middle' }}
                >
                  <div>
                    <span
                      style={{
                        fontWeight: 'bold',
                        display: 'inline-block',
                        verticalAlign: 'middle',
                        marginRight: 12
                      }}
                    >
                      <Checkbox onChange={props.toggleAmountFilter}>
                        Amount
                      </Checkbox>
                    </span>
                    <span
                      style={{
                        display: 'inline-block',
                        verticalAlign: 'middle',
                        width: 280
                      }}
                    >
                      <Slider
                        min={1}
                        range={true}
                        disabled={!props.isAmountFilterEnabled}
                        marks={marksForSlider}
                        defaultValue={props.amountFilter}
                        tipFormatter={sliderValueFormatter}
                        onAfterChange={props.onAmountFilterValueChange}
                      />
                    </span>
                  </div>
                </span>
              </div>

              <div className="filter-actions">
                <Button type="primary" onClick={props.onUpdateResults}>
                  Update
                </Button>
              </div>
            </div>
          </Panel>
        </Collapse>
      </div>

      <PaginatedList
        isVisible={Object.keys(props.donations).length > 0}
        columns={columnsToShow}
        dataSource={props.donations[pagination.current]}
        pagination={pagination}
        hasReachedEnd={isLastPageVisible}
        goToPage={props.goToPage}
      />
    </div>
  );
};

const getPagedDonations = (donations: any[]) => {
  return donations.reduce((acc: any, item: any, index: number) => {
    const page = Math.floor(index / PAGE_SIZE) + 1;
    if (!acc[page]) {
      acc[page] = [];
    }

    acc[page].push({
      ...item,
      key: index + 1
    });

    return acc;
  }, []);
};

const mapStateToProps = (state: any) => {
  return {
    donations: getPagedDonations(state.donations.donations),
    searchText: state.donations.searchText,
    amountFilter: state.donations.amountFilter,
    isAmountFilterEnabled: state.donations.isAmountFilterEnabled,
    dateFilter: state.donations.dateFilter,
    nextIdToFetch: state.donations.nextIdToFetch
  };
};

const mapDispatchToProps = (dispatcher: any) => {
  return bindActionCreators(DonationActions, dispatcher);
};

const connectComponent = connect(mapStateToProps, mapDispatchToProps);

export default compose(
  connectComponent,
  getDonationsOnMount,
  withState('pagination', 'setPagination', {
    start: 1,
    end: 10,
    current: 1
  }),
  handlers
)(render);
