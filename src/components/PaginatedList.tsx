import * as React from 'react';
import { Table } from 'antd';
import Pagination from './Pagination';

interface PaginationProps {
  start: number;
  end: number;
  current: number;
}

interface ListProps {
  isVisible: boolean;
  columns: any;
  dataSource: any;
  pagination: PaginationProps;
  hasReachedEnd: boolean;
  goToPage: any;
}

const render = (props: ListProps) => {
  return (
    <div>
      <div>
        <Table
          columns={props.columns}
          dataSource={props.dataSource}
          pagination={false}
        />
      </div>

      <div>
        <Pagination
          isVisible={props.isVisible}
          startPage={props.pagination.start}
          endPage={props.pagination.end}
          currentPage={props.pagination.current}
          hasReachedEnd={props.hasReachedEnd}
          goToPage={props.goToPage}
        />
      </div>
    </div>
  );
};

export default render;
