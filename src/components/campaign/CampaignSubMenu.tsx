import * as React from 'react';
import { Menu, Icon } from 'antd';
import { mapProps } from 'recompose';

const { SubMenu } = Menu;

const render = (props: any) => {
  return (
    <Menu
      mode="inline"
      defaultOpenKeys={['manage_campaigns']}
      style={{ height: '100%', borderRight: 0 }}
      onClick={props.handleClick}
    >
      <SubMenu
        {...props}
        key="manage_campaigns"
        title={
          <span>
            <Icon type="book" />Manage
          </span>
        }
      >
        <Menu.Item key="add">Add</Menu.Item>
      </SubMenu>
    </Menu>
  );
};

const addHandlers = mapProps((props: any) => {
  return {
    handleClick: (e: any) => {
      switch (e.key) {
        case 'add':
          props.history.push(`${props.match.path}/add`);
      }
    }
  };
});

export default addHandlers(render);
