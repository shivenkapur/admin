// tslint:disable:no-submodule-imports
// tslint:disable:no-implicit-dependencies
// tslint:disable:no-var-requires
import * as React from 'react';
import { MemoryRouter } from 'react-router';
import { Provider } from 'react-redux';
import { createMemoryHistory } from 'history';
import { mount } from 'enzyme';
import { Form } from 'antd';
import moment from '../../modules/moment';

import CampaignForm from './CampaignForm';
import * as models from '../../models';
import store from '../../modules/store';
import { getFormFieldValue } from '../../test/helpers';

function renderCampaignForm(props: any) {
  return mount(
    <Provider store={store}>
      <MemoryRouter initialEntries={['/campaigns']}>
        <CampaignForm {...props} />
      </MemoryRouter>
    </Provider>
  );
}

const campaign: models.Campaign = {
  charity: '',
  title: 'campaign title',
  details: {
    summary: 'summary',
    description: 'description',
    areaAffected: 'area affected',
    peopleAffected: 'people affected',
    helpRequired: 'help required',
    images: { main: { id: '1', fileName: '1.png' } },
    location: {
      lat: 45,
      lng: 73,
      radius: 1000,
      zoom: 12
    }
  },
  fundraising: {
    goal: {
      value: 4,
      currency: 'USD'
    }
  },
  endDate: '2018-07-08T13:29:18.118',
  active: true
};

it('renders empty form with null campaign', () => {
  const props = {
    history: createMemoryHistory(),
    onOk: jest.fn(),
    onCancel: jest.fn()
  };
  renderCampaignForm(props);
});

it('renders form populated with campaign', () => {
  const props = {
    campaign,
    history: createMemoryHistory(),
    onOk: jest.fn(),
    onCancel: jest.fn()
  };

  let wrapper = renderCampaignForm(props);
  wrapper = wrapper.find(Form);
  expect(wrapper.length).toBe(1);
  const form = wrapper.at(0);
  expect(getFormFieldValue(form, 'title')).toBe(campaign.title);
  expect(getFormFieldValue(form, 'summary')).toBe(campaign.details.summary);
  expect(getFormFieldValue(form, 'description')).toBe(
    campaign.details.description
  );
  expect(getFormFieldValue(form, 'areaAffected')).toBe(
    campaign.details.areaAffected
  );
  expect(getFormFieldValue(form, 'peopleAffected')).toBe(
    campaign.details.peopleAffected
  );
  expect(getFormFieldValue(form, 'helpRequired')).toBe(
    campaign.details.helpRequired
  );
  expect(getFormFieldValue(form, 'goal')).toBe(campaign.fundraising.goal.value);
  expect(getFormFieldValue(form, 'endDate')).toEqual(moment(campaign.endDate));
});
