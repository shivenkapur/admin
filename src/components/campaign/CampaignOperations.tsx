import * as React from 'react';
import { Route, Switch } from 'react-router';
import { withState } from 'recompose';
import CampaignList from './CampaignList';

import AddCampaign from './AddCampaign';
import EditCampaign from './EditCampaign';

const render = (props: any) => {
  const renderList = () => <CampaignList {...props} />;
  const renderAdd = () => <AddCampaign {...props} />;
  const renderViewEdit = () => <EditCampaign {...props} />;
  return (
    <div>
      <Switch>
        <Route exact={true} path="/campaigns" render={renderList} />
        <Route path="/campaigns/add" render={renderAdd} />
        <Route path="/campaigns/:id" render={renderViewEdit} />
      </Switch>
    </div>
  );
};

const addState = withState('campaign', 'setCampaign', null);

export default addState(render);
