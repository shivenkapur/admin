import * as React from 'react';
import { Route, RouteComponentProps, Switch } from 'react-router';
import { compose, withHandlers, withState } from 'recompose';

import { Button } from 'antd';

import { dispatch } from '../../modules/store';
import { campaignUpdate } from '../../actions/campaigns';
import CampaignForm from './CampaignForm';
import { Campaign } from '../../models';

interface ViewEditCampaignProps extends RouteComponentProps<any> {
  campaign: Campaign;
}

const render = (props: any) => {
  const onOk = (campaign: Campaign, file: any) =>
    dispatch(campaignUpdate(campaign, file));
  const onCancel = () => {
    props.setEditing(false, () => {
      const lastSlash = props.location.pathname.lastIndexOf('/');
      props.history.push(props.location.pathname.slice(0, lastSlash));
    });
  };

  const renderEdit = () => {
    const copyCampaign = { ...props.campaign };
    return (
      <CampaignForm
        {...{ ...props, campaign: copyCampaign }}
        onCancel={onCancel}
        onOk={onOk}
        readOnly={false}
      />
    );
  };

  const renderView = () => {
    return <CampaignForm {...props} readOnly={true} />;
  };
  return (
    <div>
      <Switch>
        <Route path={'/campaigns/:id/edit'} render={renderEdit} />
        <Route path={'/:id'} render={renderView} />
      </Switch>
      {!props.editing && (
        <Button type="primary" onClick={props.edit}>
          Edit
        </Button>
      )}
    </div>
  );
};

const addHandlers = withHandlers<any, { edit: () => void }>({
  edit: props => () => {
    props.setEditing(true, () => {
      props.history.push(`${props.location.pathname}/edit`);
    });
  }
});

const addState = withState<ViewEditCampaignProps, any, any, any>(
  'editing',
  'setEditing',
  false
);

export default compose(addState, addHandlers)(render);
