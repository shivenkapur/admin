import * as React from 'react';
import { Table, Input, Checkbox, Row, Col, Form, Button } from 'antd';
// tslint:disable-next-line
import { FormComponentProps } from 'antd/lib/form';
import { connect } from 'react-redux';
import { withHandlers } from 'recompose';
import { RouteComponentProps } from 'react-router';
import { dispatch, AdminStore } from '../../modules/store';

import { Campaign } from '../../models';
import { fetchCampaigns, deleteCampaign } from '../../actions/campaigns';

import moment from '../../modules/moment';
const FormItem = Form.Item;

type PropsIn = FormComponentProps & RouteComponentProps<any>;

interface CampaignListProps {
  campaigns: Campaign[];
  setCampaign: (campaign: Campaign) => void;
}

interface HandlerProps {
  handleClick: (e: React.FormEvent<any>) => void;
  submit: (e: React.FormEvent<any>) => void;
}

type RenderProps = PropsIn & CampaignListProps & HandlerProps;

const columns = [
  {
    title: 'Title',
    dataIndex: 'title',
    key: 'title'
  },
  {
    title: 'Charity',
    dataIndex: 'charity.name',
    key: 'charity'
  },
  {
    title: 'End Date',
    dataIndex: 'endDate',
    key: 'endDate',
    render: (date: string) => {
      return moment(date).format('MM/DD/YY');
    }
  },
  {
    title: 'Status',
    dataIndex: 'active',
    key: 'status',
    render: (active: boolean) => {
      if (active) {
        return 'Active';
      }
      return 'Expired';
    }
  },
  {
    title: 'Action',
    key: 'action',
    dataIndex: 'id',
    render: (id: string, campaign: Campaign) => {
      const deleteCampaignClick = (e: any) => {
        e.stopPropagation();
        dispatch(deleteCampaign(id));
      }
      return (
        <Button type="danger" disabled={campaign.active} onClick={deleteCampaignClick}>Delete</Button>
      )
    }
  }
];

const mapStateToProps = (state: AdminStore) => {
  state.campaigns.items = state.campaigns.items.map(item => {
    return {
      ...item,
      key: item.id
    };
  });
  return {
    campaigns: state.campaigns.items
  };
};

const render = (props: RenderProps) => {
  const { getFieldDecorator } = props.form;
  const onRowProp = (col: any, index: number) => {
    return {
      onClick: () => props.handleClick(col)
    };
  };

  return (
    <div className="campaign-search-ui">
      <Form onSubmit={props.submit}>
        <Row>
          <Col span={6}>
            <FormItem>
              {getFieldDecorator('searchString', {
                rules: []
              })(<Input placeholder="Title" />)}
            </FormItem>
          </Col>
          <Col span={6}>
            <FormItem>
              <Button type="primary" htmlType="submit">
                Refresh
              </Button>
            </FormItem>
          </Col>
        </Row>
        <Row>
          <FormItem>
            {getFieldDecorator('expired', {
              valuePropName: 'checked',
              initialValue: false
            })(<Checkbox>Include Expired</Checkbox>)}
          </FormItem>
        </Row>
      </Form>
      <Table dataSource={props.campaigns} columns={columns} onRow={onRowProp} />
    </div>
  );
};

const addHandlers = withHandlers<PropsIn & CampaignListProps, HandlerProps>({
  handleClick: props => (campaign: Campaign) => {
    props.setCampaign(campaign);
    props.history.push(`${props.location.pathname}/${campaign.id}`);
  },
  submit: props => (e: React.FormEvent<any>) => {
    e.preventDefault();
    props.form.validateFields((err: any, values: any) => {
      if (!err) {
        dispatch(fetchCampaigns(values.searchString, values.expired));
      }
    });
  }
});

const CampaignList = connect<any, PropsIn, CampaignListProps, AdminStore>(
  mapStateToProps
)(Form.create<FormComponentProps>()(addHandlers(render)));

export default CampaignList;
