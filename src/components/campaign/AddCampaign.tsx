import * as React from 'react';

import CampaignForm from './CampaignForm';
import { dispatch } from '../../modules/store';
import { campaignAdd } from '../../actions/campaigns';
import * as models from '../../models';

function render(props: any) {
  const ok = (campaign: models.Campaign, file: any) =>
    dispatch(campaignAdd(campaign, file));
  const cancel = () => {
    const lastSlash = props.location.pathname.lastIndexOf('/');
    props.history.push(props.location.pathname.slice(0, lastSlash));
  };

  return (
    <CampaignForm
      {...props}
      onOk={ok}
      onCancel={cancel}
      readOnly={false}
      campaign={null}
    />
  );
}

export default render;
