import * as React from 'react';
import { RouterProps } from 'react-router';
import { compose, defaultProps, withHandlers, lifecycle } from 'recompose';

import { Form, Input, Button, DatePicker, Col, Row } from 'antd';
// tslint:disable-next-line:no-submodule-imports
import { FormComponentProps } from 'antd/lib/form';
import moment from '../../modules/moment';

import MapZone from './MapZone';
import { FileSelect, antdFile } from '../form/FileSelect';
import * as models from '../../models';
import CharitySelect from '../charity/CharitySelect';

const FormItem = Form.Item;
const { TextArea } = Input;

const campaignZone = (campaign: models.Campaign) => {
  return {
    position: {
      lat: campaign.details.location.lat,
      lng: campaign.details.location.lng
    },
    zoom: campaign.details.location.zoom,
    radius: campaign.details.location.radius
  };
};

function render(props: RenderProps) {
  const { getFieldDecorator } = props.form;

  const endDateValidator = (rule: any, value: any, callback: any) => {
    const errors = [];
    if (!value || moment().diff(value) > 0) {
      errors.push(new Error('Please enter a valid date'));
    }
    callback(errors);
    return true;
  };

  return (
    <Form onSubmit={props.submit} className="vert-form">
      <Row>
        <Col span={8}>
          <FormItem label="Charity">
            {getFieldDecorator('charity', {
              rules: [{ required: true, message: 'Please select a charity' }],
              initialValue: props.campaign ? props.campaign.charity : null
            })(<CharitySelect readOnly={props.readOnly} />)}
          </FormItem>
          <FormItem label="Title">
            {getFieldDecorator('title', {
              rules: [{ required: true, message: 'Please provide a title' }],
              initialValue: props.campaign ? props.campaign.title : null
            })(<Input disabled={props.readOnly} />)}
          </FormItem>

          <FormItem label="Summary">
            {getFieldDecorator('summary', {
              rules: [{ required: true, message: 'Please provide a summary' }],
              initialValue: props.campaign
                ? props.campaign.details.summary
                : null
            })(<TextArea disabled={props.readOnly} />)}
          </FormItem>

          <FormItem label="Description">
            {getFieldDecorator('description', {
              rules: [
                { required: true, message: 'Please provide a description' }
              ],
              initialValue: props.campaign
                ? props.campaign.details.description
                : null
            })(<TextArea disabled={props.readOnly} />)}
          </FormItem>

          <FormItem label="Area Affected">
            {getFieldDecorator('areaAffected', {
              rules: [
                { required: true, message: 'Please specify the area affected' }
              ],
              initialValue: props.campaign
                ? props.campaign.details.areaAffected
                : null
            })(<TextArea disabled={props.readOnly} />)}
          </FormItem>

          <FormItem label="People Affected">
            {getFieldDecorator('peopleAffected', {
              rules: [
                {
                  required: true,
                  message: 'Please specify how people are affected'
                }
              ],
              initialValue: props.campaign
                ? props.campaign.details.peopleAffected
                : null
            })(<TextArea disabled={props.readOnly} />)}
          </FormItem>

          <FormItem label="Help Required">
            {getFieldDecorator('helpRequired', {
              rules: [
                { required: true, message: 'Please detail the help required' }
              ],
              initialValue: props.campaign
                ? props.campaign.details.helpRequired
                : null
            })(<TextArea disabled={props.readOnly} />)}
          </FormItem>

          <FormItem label="Goal">
            {getFieldDecorator('goal', {
              rules: [
                {
                  required: true,
                  pattern: /^[1-9]+[0-9]*$/,
                  message: 'Please enter a valid number of Dollars'
                }
              ],
              initialValue: props.campaign
                ? props.campaign.fundraising.goal.value
                : null
            })(<Input addonAfter="USD" disabled={props.readOnly} />)}
          </FormItem>

          <FormItem label="Main Image">
            {getFieldDecorator('mainImage', {
              rules: [
                {
                  required: true,
                  message: 'Please provide a campaign image file'
                }
              ],
              initialValue: props.campaign
                ? antdFile(props.campaign.details.images.main)
                : null
            })(<FileSelect readOnly={props.readOnly} />)}
          </FormItem>

          <FormItem label="End date">
            {getFieldDecorator('endDate', {
              rules: [{ required: true, validator: endDateValidator }],
              initialValue: props.campaign
                ? moment(props.campaign.endDate)
                : null
            })(<DatePicker disabled={props.readOnly} />)}
          </FormItem>
        </Col>

        <Col offset={1} span={8}>
          <FormItem label="Location">
            {getFieldDecorator('zone', {
              rules: [
                {
                  required: true,
                  message: 'Please provide a valid campaign location'
                }
              ],
              initialValue: props.campaign ? campaignZone(props.campaign) : null
            })(<MapZone readOnly={props.readOnly} />)}
          </FormItem>
        </Col>
      </Row>
      {!props.readOnly && (
        <Row gutter={10} type="flex" justify="start">
          <Col>
            <FormItem>
              <Button type="primary" htmlType="submit">
                OK
              </Button>
            </FormItem>
          </Col>
          <Col>
            <FormItem>
              <Button type="primary" onClick={props.cancel}>
                Cancel
              </Button>
            </FormItem>
          </Col>
        </Row>
      )}
    </Form>
  );
}

const formToCampaign = (props: RenderProps, values: any): models.Campaign => {
  return {
    id: props.campaign ? props.campaign.id : undefined,
    title: values.title,
    active: true, // TODO active/inactive functionality
    endDate: values.endDate,
    charity: values.charity.id ? values.charity.id : values.charity,
    details: {
      summary: values.summary,
      description: values.description,
      helpRequired: values.helpRequired,
      areaAffected: values.areaAffected,
      peopleAffected: values.peopleAffected,
      images: { main: values.mainImage },
      location: {
        lat: values.zone.position.lat,
        lng: values.zone.position.lng,
        zoom: values.zone.zoom,
        radius: values.zone.radius
      }
    },
    fundraising: {
      goal: {
        currency: 'USD',
        value: values.goal
      }
    }
  };
};

declare interface CampaignFormProps extends RouterProps {
  readOnly?: boolean;
  onOk: (campaign: models.Campaign, campaignImage: any) => void;
  onCancel: () => void;
  campaign?: models.Campaign;
}

interface HandlerProps {
  cancel: (e: React.FormEvent<any>) => void;
  submit: (e: React.FormEvent<any>) => void;
  registerRouteChangeListener: () => void;
}

type RenderProps = CampaignFormProps & FormComponentProps & HandlerProps;

type EnhancerInProps = CampaignFormProps & FormComponentProps;

const enhancer = compose<RenderProps, EnhancerInProps>(
  defaultProps({ readOnly: false, campaign: null }),
  withHandlers<EnhancerInProps, HandlerProps>({
    cancel: props => (e: React.FormEvent<any>) => {
      props.onCancel();
    },
    submit: props => (e: React.FormEvent<any>) => {
      e.preventDefault();
      props.form.validateFields((err: any, values: any) => {
        if (!err) {
          const file =
            values.mainImage.status === 'done' ? null : values.mainImage;

          const campaign: models.Campaign = formToCampaign(
            props as RenderProps,
            values
          );

          props.onOk(campaign, file);
        }
      });
    },
    registerRouteChangeListener: props => () => {
      props.history.listen((location: any, action: any) => {
        props.form.resetFields();
      });
    }
  }),
  lifecycle({
    componentWillMount() {
      (this.props as any).registerRouteChangeListener();
    }
  })
);

const formEnhancer = Form.create<CampaignFormProps>({});

export default formEnhancer(enhancer(render));
