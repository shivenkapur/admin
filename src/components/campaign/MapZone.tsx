import * as React from 'react';
import {
  compose,
  toClass,
  withHandlers,
  withState,
  defaultProps
} from 'recompose';

import { Input, Button } from 'antd';
import { Map, Circle, TileLayer, Marker, MapProps } from 'react-leaflet';
// tslint:disable-next-line:no-submodule-imports
import 'leaflet/dist/leaflet.css';

const defaultZone: Zone = {
  zoom: 11,
  radius: 1000,
  position: {
    lat: 42.8768751,
    lng: 74.5918609
  }
};

// Captures Input component strings
interface ZoneInput {
  zoom: string;
  radius: string;
  position: {
    lat: string;
    lng: string;
  };
}

interface Zone {
  zoom: number;
  radius: number;
  position: {
    lat: number;
    lng: number;
  };
}

// Tweak NaN, which lets trailing characters be
// non numerical
const numValidate = (input: string, parseFn: any): undefined | number => {
  const num = parseFn(input);
  if (isNaN(num)) {
    return;
  }
  // Last char is not a number
  const lastChar = input.charAt(input.length - 1);
  if (isNaN(parseInt(lastChar, 10))) {
    return;
  }
  return num;
};

const validateFloat = (floatStr: string): undefined | number => {
  return numValidate(floatStr, parseFloat);
};

const validateInt = (intStr: string): undefined | number => {
  return numValidate(intStr, (num: string) => parseInt(num, 10));
};

const stringifyLocation = (zone: Zone): ZoneInput => {
  const stringifyProp = (obj: any, copy: any) => {
    Object.keys(obj).forEach(key => {
      if (typeof obj[key] === 'object') {
        copy[key] = {};
        stringifyProp(obj[key], copy[key]);
        return;
      }
      copy[key] = `${obj[key]}`;
    });
  };
  const stringified: any = {};
  stringifyProp(zone, stringified);
  return stringified;
};

const validateInput = (zone: ZoneInput): Zone | undefined => {
  const pos = zone.position;
  const zoom = zone.zoom;
  const radius = zone.radius;
  if (
    validateFloat(pos.lat) &&
    validateFloat(pos.lng) &&
    validateInt(zoom) &&
    validateInt(radius)
  ) {
    return {
      zoom: parseInt(zoom, 10),
      radius: parseInt(radius, 10),
      position: {
        lat: parseFloat(pos.lat),
        lng: parseFloat(pos.lng)
      }
    };
  }
};

const composer = compose(
  // State of the input fields
  defaultProps({ readOnly: Boolean }),
  withState('zoneInput', 'setZoneInput', (propsIn: any) => {
    if (propsIn.value) {
      return stringifyLocation(propsIn.value);
    }
    return stringifyLocation(defaultZone);
  }),

  withState('inputError', 'setInputError', false),
  // State of the leaflet map location
  withState('zone', 'setZone', (propsIn: any) => {
    if (propsIn.value) {
      return propsIn.value;
    }
    return defaultZone;
  }),
  // Make handler available to inner component
  withHandlers({
    triggerChange: (propsIn: any) => (changedValue: any) => {
      const onChange = propsIn.onChange;
      if (onChange) {
        onChange(changedValue);
      }
    }
  }),
  withHandlers({
    handleLatChange: (propsIn: any) => (e: Event) => {
      const lat = (e.target as any).value;
      const position = { lat, lng: propsIn.zoneInput.position.lng };
      propsIn.setZoneInput({ ...propsIn.zoneInput, position });
    },
    handleLngChange: (propsIn: any) => (e: Event) => {
      const lng = (e.target as any).value;
      const position = { lng, lat: propsIn.zoneInput.position.lat };
      propsIn.setZoneInput({ ...propsIn.zoneInput, position });
    },
    handleZoomChange: (propsIn: any) => (e: Event) => {
      const zoom = (e.target as any).value;
      propsIn.setZoneInput({ ...propsIn.zoneInput, zoom });
    },
    handleRadiusChange: (propsIn: any) => (e: Event) => {
      const radius = (e.target as any).value;
      propsIn.setZoneInput({ ...propsIn.zoneInput, radius });
    },
    handleMapUpdate: (propsIn: any) => (e: Event) => {
      const zone = validateInput(propsIn.zoneInput);
      if (zone) {
        propsIn.setInputError(false);
        propsIn.setZone(zone);
        propsIn.triggerChange(zone);
      } else {
        propsIn.setInputError(true);
        propsIn.triggerChange(null);
      }
    }
  })
);

// Props not in TypeScript definition
// @types/react-leaf
interface ExtraMapProps {
  viewport: any;
}

const MapZone = (props: any) => {
  const marker = <Marker position={props.zone.position} />;

  const propsMap: MapProps & ExtraMapProps = {
    zoom: props.zone.zoom,
    center: props.zone.position,
    viewport: props.zone, // dynamic property to change zoom, center not specified
    dragging: false,
    zoomControl: false
  };

  const svgSupport = () => {
    return !!(
      document.createElementNS &&
      document.createElementNS('http://www.w3.org/2000/svg', 'svg')
        .createSVGRect
    );
  };

  return (
    <div>
      <label className="ant-form-item-control">Latitude</label>
      <Input
        placeholder="Latitude"
        value={props.zoneInput.position.lat}
        onChange={props.handleLatChange}
        disabled={props.readOnly}
      />
      <label className="ant-form-item-control">Longitude</label>
      <Input
        placeholder="Longitude"
        value={props.zoneInput.position.lng}
        onChange={props.handleLngChange}
        disabled={props.readOnly}
      />
      <label className="ant-form-item-control">Zoom</label>
      <Input
        placeholder="Zoom"
        value={props.zoneInput.zoom}
        onChange={props.handleZoomChange}
        disabled={props.readOnly}
      />
      <label className="ant-form-item-control">Radius (meters)</label>
      <Input
        placeholder="Radius"
        value={props.zoneInput.radius}
        onChange={props.handleRadiusChange}
        disabled={props.readOnly}
      />

      <Button
        type="primary"
        onClick={props.handleMapUpdate}
        disabled={props.readOnly}
      >
        Update
      </Button>
      <Map style={{ height: '400px', width: '400px' }} {...propsMap}>
        <TileLayer
          attribution="&amp;copy <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        {svgSupport() && (
          <Circle
            radius={props.zone.radius}
            center={props.zone.position}
            color="red"
            fillColor="red"
          />
        )}
        {marker}
      </Map>
    </div>
  );
};

export default composer(toClass(MapZone));
export { defaultZone };
