import * as React from 'react';
import { WebAuth } from 'auth0-js';

declare interface LockProps {
  lock: WebAuth;
}

function Lock(props: LockProps) {
  return <script>{props.lock.authorize()};</script>;
}

export default Lock;
