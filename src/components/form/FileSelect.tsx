import * as React from 'react';
import { Button, Icon, Upload } from 'antd';
// tslint:disable-next-line:no-submodule-imports
import { UploadFile, UploadProps } from 'antd/lib/upload/interface';
import { compose, defaultProps, withState, withHandlers } from 'recompose';

// This component wraps the Antd Upload component to provide a simple file selection component.

// TODO FileUpload component works with instances of UploadFile. Instead of fudging existing
// uploads into the shape of a FileUpload, existing uploads should be shown seperately
// and the validation tweaked so that if there is an existing file it is not necessarry
// to choose a new file.

type FileOrFudge = UploadFile | any;

function render(props: any) {
  const uploadProps: UploadProps = {
    listType: 'picture',
    fileList: props.value !== null ? [props.value] : [],
    beforeUpload: props.beforeUpload,
    onRemove: props.onRemove,
    accept: '.png,.jpg,.jpeg'
  };

  return (
    <Upload {...uploadProps}>
      <Button disabled={props.readOnly}>
        <Icon type="upload" />Select
      </Button>
    </Upload>
  );
}

const addProps = defaultProps({ disabled: false });

// tweak the behaviour of the FileUpload component to disable
// its ajax uploads, have a max fileList size of 1, and obey
// the readOnly property
const addHandlers = withHandlers({
  onRemove: (props: any) => (file: FileOrFudge) => {
    if (props.readOnly) {
      return false;
    }
    props.onChange(null);
    props.setFileList((files: FileOrFudge[]) => {
      return [];
    });
    return true;
  },
  beforeUpload: (props: any) => (file: any) => {
    props.onChange(file);

    // maintain the component's internal file list
    props.setFileList((fileList: FileOrFudge[] = []) => {
      const newFileList = fileList.splice(0, 0);
      newFileList.push(file);
      return newFileList;
    });

    return false;
  }
});

const addStates = withState('fileList', 'setFileList', (props: any) => {
  return props.value
    ? [
        {
          uid: props.value.uid,
          name: props.value.fileName,
          status: 'done'
        }
      ]
    : [];
});

const FileSelect = compose(addProps, addStates, addHandlers)(render);

function antdFile(file: any) {
  return {
    ...file,
    url: '',
    name: file.fileName,
    uid: file.id,
    status: 'done'
  };
}

export { FileSelect, FileOrFudge, antdFile };
