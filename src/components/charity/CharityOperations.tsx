import * as React from 'react';
import { Route, Switch } from 'react-router';
import { withState } from 'recompose';

import CharityList from './CharityList';
import AddCharity from './AddCharity';
import ViewEditCharity from './ViewEditCharity';

const render = (props: any) => {
  const renderViewEdit = () => <ViewEditCharity {...props} />;
  const renderList = () => <CharityList {...props} />;
  const renderAdd = () => <AddCharity {...props} />;
  return (
    <div>
      <Switch>
        <Route exact={true} path="/charities" render={renderList} />
        <Route path="/charities/add" render={renderAdd} />
        <Route path="/charities/:id" render={renderViewEdit} />
      </Switch>
    </div>
  );
};

const addState = withState('charity', 'setCharity', {});

export default addState(render);
