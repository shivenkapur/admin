import * as React from 'react';
import { Table } from 'antd';
import { connect } from 'react-redux';
import { withHandlers } from 'recompose';
import { RouteComponentProps, RouterProps } from 'react-router';

import { AdminStore } from '../../modules/store';
import { Charity } from '../../models';

interface CharityListProps {
  charities: Charity[];
  setCharity: (charity: Charity) => void;
}

const columns = [
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name'
  },
  {
    title: 'Description',
    dataIndex: 'description',
    key: 'description'
  }
];

const mapStateToProps = (state: AdminStore) => {
  // Table requires a key field on each row
  state.charities.items = state.charities.items.map(item => {
    return {
      ...item,
      key: item.id
    };
  });
  return {
    charities: state.charities.items
  };
};

const render = (props: any) => {
  const onRowProp = (col: any, index: number) => {
    return {
      onClick: () => props.handleClick(col)
    };
  };
  return (
    <Table dataSource={props.charities} columns={columns} onRow={onRowProp} />
  );
};

const addHandlers = withHandlers<any, any>({
  handleClick: (
    props: RouteComponentProps<any> & RouterProps & CharityListProps
  ) => (charity: Charity) => {
    props.setCharity(charity);
    props.history.push(`${props.location.pathname}/${charity.id}`);
  }
});
const CharityList = connect<any, any, CharityListProps, any>(mapStateToProps)(
  addHandlers(render)
);

export default CharityList;
