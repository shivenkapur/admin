import * as React from 'react';
import { Form, Select } from 'antd';
import { connect } from 'react-redux';
import { compose, withHandlers, lifecycle, defaultProps } from 'recompose';

import { dispatch } from '../../modules/store';
import { Charity } from '../../models';
import { fetchCharities } from '../../actions/charity';

const Option = Select.Option;

function render(props: any) {
  const charities: any[] = [];

  props.charities.forEach((charity: Charity) => {
    charities.push(
      <Option key={charity.id} value={charity.id}>
        {charity.name}
      </Option>
    );
  });

  return (
    <Select
      disabled={props.readOnly}
      defaultValue={props && props.value ? props.value.name : null}
      onChange={props.handleOnChange}
    >
      {charities}
    </Select>
  );
}

const enhance = compose(
  lifecycle({
    componentDidMount() {
      dispatch(fetchCharities());
    }
  }),
  defaultProps({ charities: [] }),
  withHandlers({
    handleOnChange: (propsIn: any) => (value: string) => {
      propsIn.onChange(value);
    }
  })
);

const CharitySelect = connect<any, any, any, any>((state: any) => {
  return {
    charities: state.charities.items
  };
}, null)(enhance(render));

export default Form.create<any>({})(CharitySelect);
