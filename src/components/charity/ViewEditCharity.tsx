import * as React from 'react';
import { Route, RouteComponentProps, Switch } from 'react-router';
import { compose, withHandlers, withState } from 'recompose';

import { Button } from 'antd';

import { dispatch } from '../../modules/store';
import { updateCharity } from '../../actions/charity';
import CharityForm from './CharityForm';
import { Charity } from '../../models';

interface ViewEditCharityProps extends RouteComponentProps<any> {
  charity: Charity;
}

const render = (props: any) => {
  const validCharity = (charity: Charity, file: any) =>
    dispatch(updateCharity(charity, file));
  const onCancel = () => {
    props.setEditing(false, () => {
      const lastSlash = props.location.pathname.lastIndexOf('/');
      props.history.push(props.location.pathname.slice(0, lastSlash));
    });
  };

  const renderEdit = () => {
    const copyCharity = { ...props.charity };
    return (
      <CharityForm
        {...{ ...props, charity: copyCharity }}
        onCancel={onCancel}
        validCharity={validCharity}
        readOnly={false}
      />
    );
  };

  const renderView = () => {
    return <CharityForm {...props} readOnly={true} />;
  };
  return (
    <div>
      <Switch>
        <Route path={'/charities/:id/edit'} render={renderEdit} />
        <Route path={'/:id'} render={renderView} />
      </Switch>
      {!props.editing && (
        <Button type="primary" onClick={props.edit}>
          Edit
        </Button>
      )}
    </div>
  );
};

const addHandlers = withHandlers<any, { edit: () => void }>({
  edit: props => () => {
    props.setEditing(true, () => {
      props.history.push(`${props.location.pathname}/edit`);
    });
  }
});

const addState = withState<ViewEditCharityProps, any, any, any>(
  'editing',
  'setEditing',
  false
);

export default compose(addState, addHandlers)(render);
