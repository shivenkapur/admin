import * as React from 'react';

import CharityForm from './CharityForm';
import { Charity } from '../../models';
import { dispatch } from '../../modules/store';
import { addCharity } from '../../actions/charity';

function render(props: any) {
  const validCharity = (charity: Charity, file: any) =>
    dispatch(addCharity(charity, file));
  const onCancel = () => {
    const lastSlash = props.location.pathname.lastIndexOf('/');
    props.history.push(props.location.pathname.slice(0, lastSlash));
  };
  return (
    <CharityForm
      {...props}
      charity={null}
      readOnly={false}
      validCharity={validCharity}
      onCancel={onCancel}
    />
  );
}

export default render;
