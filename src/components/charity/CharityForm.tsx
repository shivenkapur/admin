import * as React from 'react';
import { RouterProps } from 'react-router';

import { Form, Input, Button, Col, Row } from 'antd';
import { withHandlers, compose, lifecycle } from 'recompose';

import { FileSelect, antdFile } from '../form/FileSelect';
// tslint:disable-next-line:no-implicit-dependencies
import { Charity } from '../../models';

const FormItem = Form.Item;
const { TextArea } = Input;

declare interface CharityFormProps extends RouterProps {
  readOnly: boolean;
  // Function called after OK pressed and successful validation of Charity
  validCharity: (charity: Charity, file: any) => void;
  charity?: Charity;
}

function render(props: any) {
  const { getFieldDecorator } = props.form;

  return (
    <Form onSubmit={props.submit} className="vert-form">
      <FormItem label="Name">
        {getFieldDecorator('name', {
          rules: [{ required: true, message: 'Please provide a name' }],
          initialValue: props.charity ? props.charity.name : null
        })(<Input disabled={props.readOnly} />)}
      </FormItem>

      <FormItem label="Description">
        {getFieldDecorator('description', {
          rules: [{ required: true, message: 'Please provide a description' }],
          initialValue: props.charity ? props.charity.description : null
        })(<TextArea disabled={props.readOnly} />)}
      </FormItem>

      <FormItem label="Logo">
        {getFieldDecorator('logo', {
          rules: [
            { required: true, message: 'Please provide a charity logo file' }
          ],
          initialValue: props.charity ? antdFile(props.charity.logo) : null
        })(<FileSelect readOnly={props.readOnly} />)}
      </FormItem>
      {!props.readOnly && (
        <Row gutter={10} type="flex" justify="start">
          <Col>
            <FormItem>
              <Button type="primary" htmlType="submit">
                OK
              </Button>
            </FormItem>
          </Col>
          <Col>
            <FormItem>
              <Button type="primary" onClick={props.cancel}>
                Cancel
              </Button>
            </FormItem>
          </Col>
        </Row>
      )}
    </Form>
  );
}

const addHandlers = withHandlers({
  cancel: (props: any) => (e: any) => {
    props.onCancel();
  },
  submit: (props: any) => (e: any) => {
    e.preventDefault();
    props.form.validateFields((err: any, values: any) => {
      if (!err) {
        const toUpload = values.logo.status === 'done' ? null : values.logo;

        const charity: Charity = {
          id: props.charity ? props.charity.id : null,
          name: values.name,
          description: values.description,
          logo: toUpload ? null : props.charity.logo
        };
        props.validCharity(charity, toUpload);
      }
    });
  },
  registerRouteChangeListener: (props: any) => () => {
    props.history.listen((location: any, action: any) => {
      props.form.resetFields();
    });
  }
});

const addLifecycle = lifecycle<any, any>({
  componentWillMount() {
    this.props.registerRouteChangeListener();
  }
});

const enhance = compose<any, any>(addHandlers, addLifecycle);

const CharityForm = Form.create<CharityFormProps>({})(enhance(render));
export default CharityForm;
