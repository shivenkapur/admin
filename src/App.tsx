import * as React from 'react';
import { Router, Route, Redirect } from 'react-router-dom';
import { Switch } from 'react-router';
import { Provider } from 'react-redux';

import { WebAuth } from 'auth0-js';
import Lock from './components/Lock';

import './App.css';
import log from './modules/log';
import config from './modules/config';
import history from './modules/history';
import * as auth from './modules/auth';
import Main from './components/Main';
import store from './modules/store';
import { login } from './actions/user';

const lock: WebAuth = auth.newWebAuth();

const Login = () => <Lock lock={lock} />;

const LoginError = () => <div>Error logging in.</div>;

const LoggedOut = () => (
  <div>
    <div>You are now logged out.</div>
    <a href="/login">Log in again.</a>
  </div>
);

// Unable to make TS happy with spread in object deconstruction.
// const PrivateRoute = ({ Comp, ...rest }: { Comp: ComponentClass | ClassicComponentClass  }) => {
// tslint:disable-next-line
const PrivateRoute = ({ Comp, ...rest }: any) => {
  const render = (props: any) => {
    return auth.isAuthenticated(store.getState().user) ? (
      <Comp {...rest} history={props.history} />
    ) : (
      <Redirect
        to={{
          pathname: '/login',
          state: { from: props.location }
        }}
      />
    );
  };

  return <Route {...rest} render={render} />;
};

const App = () => {
  if (!config.SECURED) {
    log.debug('setting articial logged in user');
    login({
      name: 'Admin',
      IDToken: '',
      accessToken: ''
    });
  }
  const renderAuth0 = (props: any) => {
    if (/access_token|id_token|error/.test(props.location.hash)) {
      auth.handleAuthentication(lock, store, props.history);
    }
    return <div>Handling auth0 response...</div>;
  };
  return (
    <Provider store={store}>
      <Router history={history}>
        <Switch>
          <Route path="/auth0" render={renderAuth0} />"
          <Route path="/login" component={Login} />
          <Route path="/loggedOut" component={LoggedOut} />
          <Route path="/loginError" component={LoginError} />
          <PrivateRoute Comp={Main} path="/" store={store} />
        </Switch>
      </Router>
    </Provider>
  );
};

export default App;
