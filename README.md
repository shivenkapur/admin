# OneRelief Admin App

## Overview

The OneRelief Admin app is a single-page-application based on React.

The Admin app is principally used to manage the campaigns that are shown to users of the donation webapp; manage the charity organizations we support and monitor donations made through the webapp.

The following key technologies are used:

* [TypeScript](https://www.typescriptlang.org/)
* [React](https://reactjs.org/docs/)
* [Redux](https://redux.js.org/)
* [React Router](https://reacttraining.com/react-router/)
* [AntD](https://ant.design/docs/react/introduce)
* [recompose](https://github.com/acdlite/recompose)
* [Architecture](https://auth0.com/docs/architecture-scenarios/application/spa-api)
* [create-react-app-buildpack](https://github.com/mars/create-react-app-buildpack)
* [Jest](https://facebook.github.io/jest/)

TBC

### Auth0

TBC

## Developer Guidelines

### TypeScript

#### Type Definitions General

TBC

#### Component Props

TBC

#### Component State

TBC

#### Redux State

TBC

#### Redux Actions

TBC

### Component Creation

Use of ES6 classes should be [avoided](http://blog.krawaller.se/posts/5-reasons-not-to-use-es6-classes-in-react/) Use the [recompose](https://medium.com/javascript-inside/why-the-hipsters-recompose-everything-23ac08748198) library to create stateful components and more.

### Yarn

| Command        | Description                                     |
| -------------- | ----------------------------------------------- |
| `yarn install` | Install the project's NPM dependencies          |
| `yarn develop` | Start an incremental build server               |
| `yarn test`    | Start a test runner in watch mode               |
| `yarn build`   | Create a production build in the `build` folder |

## Docker Deployment

### Image

Building the Docker image will copy a production build from the `build` folder.

### Run

To run the Docker image:

* Supply a port mapping for Node.js server

For example, where `admin` is the name of the Docker image, and `admin` is the intended name of the container:

`docker run -p 9000:9000 --name admin admin`

## Learning

* [Redux videos](https://egghead.io/lessons/react-redux-the-single-immutable-state-tree)
