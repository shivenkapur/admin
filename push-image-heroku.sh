#!/bin/bash

# build image and push to Heroku. requires prior Heroku login
# heroku container:login

# first argument is name of Heroku app

docker build . -f Dockerfile -t admin
docker tag admin registry.heroku.com/$1/web
docker push registry.heroku.com/$1/web
