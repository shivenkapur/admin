FROM alpine:3.7

EXPOSE 9000

RUN apk update && \
  apk add nodejs bash yarn

RUN yarn global add pushstate-server

RUN mkdir /app

# copy into the container the pre-built production build
COPY ./build /app/build
WORKDIR /app

CMD export NODE_ENV=production && \
  pushstate-server build $(if [ $PORT ] ;then echo $PORT; else echo "9000"; fi)
