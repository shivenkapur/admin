#! bin/bash

# remove nodejs typings
if [[ -e ./node_modules/@types/node ]] ;then rm -r ./node_modules/@types/node; fi

# antd expecting moment.js to be ES6 compliant, but it's not. exports bare function
echo "patching antd..."
grep -lr "import \* as moment" ./node_modules/antd | xargs sed -i'' 's/import \* as moment/import moment/g'