#!/bin/bash

echo "It is important here that env var HEROKU_API_KEY is set correctly: $HEROKU_API_KEY"
heroku -v
heroku container:login

env | sed -n -e 's/REACT_/REACT_/pg' | sed -e 's/ /\n/g' > .env
echo $(less .env)
(yarn install && yarn build) || exit 1

heroku container:push -a $HEROKU_APP_NAME web
heroku container:release -a $HEROKU_APP_NAME web
