import * as antd from 'antd';

// unplug the notifications, generating Promise warnings: unhandled rejections
const notification = { error: jest.fn(), info: jest.fn() };

export * from 'antd';
export { notification };
